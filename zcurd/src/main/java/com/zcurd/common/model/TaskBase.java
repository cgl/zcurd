package com.zcurd.common.model;

import java.util.Map;

import com.zcurd.common.DbMetaTool;
import com.zcurd.common.model.base.BaseTaskBase;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class TaskBase extends BaseTaskBase<TaskBase> {
	public static final TaskBase dao = new TaskBase().dao();

	public Map<String, Object> getDictDatatarget_type() {
		return DbMetaTool.getDictData("select dict_key, dict_value from sys_dict where dict_type='task_type'");
	}

	public Map<String, Object> getDictDatastatus() {
		return DbMetaTool.getDictData("select dict_key, dict_value from sys_dict where dict_type='task_statu'");
	}
}
